+++
title = "{{ replace (replaceRE "[^a-zA-Z]+(.*)" "$1" .Name) "-" " " | title }}"
date = "{{ .Date }}"
hidedates = ["lastmod"]
slug =  "{{ replaceRE "[^a-z]+(.*)" "$1" .Name }}"
license = "cc-by"
draft = false
+++

