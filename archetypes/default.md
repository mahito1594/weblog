+++
title = "{{ replace (replaceRE "[^a-zA-Z]+(.*)" "$1" .Name) "-" " " | title }}"
date = "{{ .Date }}"
hidedates = ["lastmod"]
draft = false
+++

