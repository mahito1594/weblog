+++
title = "straight.el でインストールしたパッケージの README を開く (ただし不完全)"
date = "2020-01-31T20:24:58+09:00"
hidedates = ["lastmod"]
slug =  "straight-view-readme"
tags = ["emacs"]
license = "cc-by"
draft = false
+++

タイトルの通り，[straight.el](https://github.com/raxod502/straight.el) を用いてインストールした Emacs パッケージの README を (`view-mode` で) 開く関数を書いてみました．
元ネタはこちらの[るびきちさんの記事](http://emacs.rubikitch.com/helm-quelpa/)です．
ただし筆者の ELisp 力不足のために，元ネタと違って README を開くことしかできません．
また，後述するように若干不完全です．

以前から自分の `init.el` に記述していたのですが，大きな問題はなさそうなので本ブログにも書いてみます．

## `my-straight-view-readme`
`M-x my-straight-view-readme` でインタラクティブに使用できます．
関数の定義は次のとおりです． `cl-lib` が必要になります．

``` emacs-lisp
(defvar my-straight-readme-names-list
  '("README" "README.org"
    "README.md" "README.mkdn" "README.mdown")
  "The list used by `my-straight-view-readme'.")

(defun my-straight-view-readme (package)
  "Open README of PACKAGE in `view-mode' if it exists.

We search a file such as \"README\", \"README.org\" and so on.
We remark that search case-insensitively.
See `my-straight-readme-name-list'."
  (interactive (list (straight--select-package "View README" nil 'installed)))
  (let ((found nil))
    (cl-loop with case-fold-search = t
             for file in my-straight-readme-names-list
             for path = (expand-file-name file (straight--repos-dir package))
             when (file-exists-p path)
             return (progn
                      (setq found t)
                      (view-file path)))
    (unless found (message "README not found."))))
```

`cl-loop` の使い方がよくわからなかったので見様見真似です．
元ネタのるびきちさんの記事では [Helm](https://github.com/emacs-helm/helm) を使っていましたが，ここでは `straight.el` が用意している `straight--select-package` という関数を利用しています．

指定されたパッケージがインストールされたディレクトリ内の，`README` ファイルを探します．
探すファイル名は `my-straight-readme-names-list` 変数で指定しています．

### 既知の問題点
パッケージがダウンロードされたディレクトリを得るために `straight--repos-dir` 関数を利用しています．
これはパッケージ名がリポジトリ名と異なる場合に問題を引き起こします．

例えば

``` emacs-lisp
(straight--repos-dir "straight")
; => ~/.emacs.d/straight/repos/straight/
; 正しいダウンロード先のパスは
;    ~/.emacs.d/straight/repos/straight.el/
```

となるため， `my-straight-view-readme` では `straight` の README を開くこととができません．

## `straight.el` とは？
日本語で `straight` について言及している記事が少ない (し，内容があまり無かったり古かったりする)ように思えるので，軽く紹介をしたいと思います．
README を読むのが一番早いし確実だと思うので，したがって以下の記述にはあまり価値がありません．
また多分に誤りを含みます．

### インストール
`straight` 自体のインストールは [README](https://github.com/raxod502/straight.el#getting-started) に書いてあるようにできます．
後述するように version lock ファイルを利用できるので `master` ブランチのものよりも `develop` ブランチのものをインストールしたほうが良いように思えます[^1]．

`straight` のインストール前に `(setq straight-repository-branch "develop")` とするだけです．

### 基本的な使い方
`straight-use-package` 関数を使ってパッケージをインストールできます．
インタラクティブにも使えますが，[`use-package`](https://github.com/jwiegley/use-package) などと同時に使うことが多いと思います
(参考: [grugrut さんの記事](http://grugrut.hatenablog.jp/entry/2019/06/04/224847))．

インストール時にブランチを指定したりできます．
詳しくは [README](https://github.com/raxod502/straight.el#the-recipe-format) を参照してください．

### バージョン固定
`M-x straight-freeze-versions` とすることで，現在インストールされているバージョンから version lock ファイル (デフォルトだと `~/.emacs.d/straight/versions/default.el`) が作成されます[^2]．
逆に version lock ファイルで指定されたバージョンをインストールするには `M-x straight-thaw-versions` を実行します．
例によって詳しくは README を参照してください．

特定のパッケージのバージョンを変更はインストールされたディレクトリ (`~/.emacs.d/straight/repos/` 以下) に移動して `git reset --hard` などで行えます[^3]．
その後 `straight-rebuild-package` や `straight-normalize-package` 等を実行します．

最新版にアップデートするだけならば `straight-pull-package`/`straight-pull-package-and-deps`/`straight-pull-all` などを実行することでも可能です．

### Tips
#### 起動が遅い
[FAQ: My init time got slower](https://github.com/raxod502/straight.el#my-init-time-got-slower) を参照．

#### Org mode のインストール
過去には `straight` で Org mode をインストールするためには若干の小細工が必要だったようです ([参考](https://www.mhatta.org/wp/2018/09/23/org-mode-101-6/)) が，
[2019年3月15日](https://github.com/raxod502/straight.el#march-15-2019)よりその必要はなくなりました．

#### shallow clone
`:depth` キーワード，あるいは `straight-vc-git-default-clone-depth` 変数を `1` にすることで shallow clone となります．
ただし version lock ファイルで固定されている場合 `straight-vc-git-default-clone-depth` 変数は影響しません．

[^1]: 私見です

[^2]: パッケージ名とコミットハッシュのドット対からなるリスト

[^3]: 他に良い方法があるかもしれないですが，README を読んだ限り見つかりませんでした
