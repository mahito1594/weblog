+++
title = "Ton Koopman, The Amsterdum Baroque Orchestra & Choir による Bach のカンタータ全集を買った"
date = "2020-03-23T21:57:38+09:00"
hidedates = ["lastmod"]
slug =  "bought-bach-cantatas"
tags = ["diary"]
license = "cc-by"
draft = false
+++

タイトルの通り，J.S. Bach のカンタータ全集(Ton Koopman 指揮，The Amsterdum Baroque Orchestra & Choir) を買った．
学部生の時分に買おうと思っていたのだけれど，いつの間にか在庫切れになっていて悔しい思いをした．
今回，再販されたのを見つけてうっかり購入してしまった．
財布がかなり軽くなってしまったが，仕方ないとしよう．

音楽には全く詳しくは無いのだけど，Koopman の Bach は軽やかな感じがして聴いていてとても楽しい．
Herreweghe の Bach も好きなのだけど，こちらはどうにも聴くのに体力が必要になる．
特にマタイ受難曲．
鈴木雅明の録音はちゃんと聴いたことがないけど，いずれ聴いてみたいと思っている．
昨年末に BCJ と鈴木雅明による Messiah (G. F. Handel) を聴きに行ったけれど非常に良かったので．

リッピングした楽曲データをどう管理するかしばらく悩んでいたけど，ようやく方針が立ったので早速カンタータを聴いていこうと思っている．
