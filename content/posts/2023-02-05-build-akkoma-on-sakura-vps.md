+++
title = "Akkoma サーバをたてて Fediverse デビューした"
date = "2023-02-05T18:05:42+09:00"
hidedates = ["lastmod"]
slug = "build-akkoma"
tags = ["diary"]
license = "cc-by"
draft = false
+++

Twitter の人たちが避難の準備を始めているようなので、自分も Fediverse デビューしてみた。
折角なので既存のインスタンスに参加するのではなく、お一人様用サーバを立ててみようかなと思ったのでそのメモを残しておこうと思う。

有名なのは [Mastodon](https://joinmastodon.org) だけど、
なんとなく別のソフトウェアを使った。

まぁちゃんと理由はあって、Mastodon は結構重いという話を聞いたから他の Fediverse マイクロブログを探して、次のどれかにしてみようと思った：

- [Misskey](https://join.misskey.page/ja-JP/)
- [Pleroma](https://pleroma.social/)
- [GNU social](https://gnusocial.network/)

Elixir に興味があったので、Pleroma をインストールしてみようと思ったんだけどインストールの途中で失敗するので、
最終的に Pleroma の fork である [Akkoma](https://akkoma.social/) になった。

立てたサーバはこちら、興味があればどうぞ[^1]。

- [Ringed Space](https://ringed.space)

とはいえインストール自体は [Installation Guide](https://docs.akkoma.dev/stable/installation/otp_en/) 通りにやれば普通にできた（たまに typo があるけど）。
自分は Ubuntu 22.04 に OTP リリースをいれた。
サーバはさくらインターネットの VPS を借りた[^2]。

詰まったところと言えばうっかり ufw でポート塞いでいるのを忘れていて nginx が上手く動かせてなかったりくらい。
設定変えた後はきちんと `ufw reload` しましょう。

あと Linux を触るのが久しぶりでユーザ追加したり、ログインシェルを切り替える方法を完全に忘れていて困った。

なんだかんだ Twitter はだらだら見続けるような気がするけど、
投稿は Akkoma の方に寄せていくかもしれない。
知らんけど。

[^1]: セキュリティの設定とか見よう見まねでやっているので攻撃するのはやめてくださいね
[^2]: メールサーバをさくらインターネットで借りているのでその流れで。石狩リージョンの RAM 1GB のやつ