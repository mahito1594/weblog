+++
title = "楽曲ファイルの管理・再生について"
date = "2020-03-23T22:17:13+09:00"
hidedates = ["lastmod"]
slug =  "how-to-manage-audio-files"
tags = ["memo"]
license = "cc-by"
draft = false
+++

## TL; DR
購入した CD を flac 形式でリッピングして，それを適当に管理する方法について以下のように方針を定めた：

- リッパーは [X Lossless Decoder](https://tmkk.undo.jp/xld/ "XLD")
- 形式は flac
- メタデータは(必要に応じて) [MusicBrainz Picard](https://picard.musicbrainz.org/ "MusicBrains Picard") で編集する
- データの保存先には [pCloud](https://pcloud.com "pCloud") を利用する
- 再生は [cmus](https://cmus.github.io/ "cmus")
- ファイル名・ディレクトリ名は適当なスクリプトを使う

Apple Lossless ではなく flac を利用することについては，特に理由があるわけではなく，なんとなく．
X Lossless Decoder や cmus については検索したり，`README` や `man` を読んでください．

## ファイル名・ディレクトリ構造
ファイル名については，当初は `"<Album Artist>/<Album Title> <Disc Number>/<Track Number> <Track Title>.flac"` の形にしようかと思っていましたが，結局やめました．
理由はファイル名(ディレクトリ名)に非 ascii なアクセント付文字を持たせたくなかったからです．
(どうにも `rsync -av --delte` で楽曲データを移そうとすると，アクセント付き文字を名前に持つファイルが一旦削除されてから再転送されてしまい困りました．)

ファイルのメタデータが正しければそうそう問題は無いはずなので，ファイル名は `"Disc <Disc Number> - Track <Track Number>.flac"` の形にすることにしました．

加えて次の理由から `rename-flac` というシェルスクリプトを書いて楽曲ファイルをリネームすることにしました．

- 既に取り込み済みの flac ファイルがある
- Album Artist や Album Title にアクセント付き文字が含まれている場合がある．

次のスクリプトは，与えられたファイルを

- メタデータから `"<Album Artist>/<Album Title>/Disc <Disc Number> - Track <Track Number>.flac"` にリネーム
- Album Artist および Albu Title からアクセント付き文字を ascii 文字に変換
- `/` や `:` などのヤバそうな文字を適当に置換

します．
あまり考えずに書きなぐったのであまりよろしくないと思いますが，ひとまずはこれで期待した結果になりました．

{{< gist mahito1594 5eef132caf5b1d10e0a08d32ca34b600>}}

`unaccent` は，macOS の場合 `brew install unac` でインストールできます．

ディレクトリ内のすべての flac ファイルをリネームするのは以下のワンライナーで実現できます[^1]．

``` shell
find <path to MUSIC_LIBRARY> -type d -print0 | xargs -0 -t -I{} bash -c 'cd "{}" && for file in *.flac; do renam-flac "$file"; done
```

リネーム後，空のディレクトリは `find <path to MUSIC_LIBRARY> -type d -empty -delete` で削除できます．

[^1]: 参考 <https://askubuntu.com/questions/589321/use-metaflac-to-rename-flac-files-from-flac-tags-recursively>
