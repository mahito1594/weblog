+++
title = "My First Post with Hugo"
date = "2019-11-09"
slug =  "my-first-post-hugo"
draft = false
license = "cc-by"
hidedates = ["lastmod"]
math = true
+++

静的サイトジェネレータである [Hugo](https://gohugo.io) を利用して個人サイトを作成してみました．
Hugo に関しては詳しいわけでもないですし，日本語でも様々な記事がありますので割愛します．
本記事は個人的な設定等のメモ書きになります．

[ezhil](https://github.com/vividvilla/ezhil) というテーマを基に次のような個人的な改良[^1]を行いました．

- 多言語対応
- 数式の表示
- 記事ごとのライセンス
- 投稿・更新日時の表示

fork したリポジトリは[こちら](https://github.com/mahito1594/ezhil)です．

### 多言語対応
日本語と英語のページを表示できるようにしました．見様見真似なので不十分な箇所が多くあると思いますが，一応満足はしています．

### 数式の表示
[KaTeX](https://katex.org/) を利用して数式の描画を行います．インライン数式は `\\(` と`\\)` で，ディスプレイ数式は `\\[` と `\\]` で囲みます．

#### 例
いくつかの文字，例えば下付き添字の `_` などは Markdown のマークアップでないことを示すためにエスケープする必要があります．

```
例えば \\(y^2 = x^3 + ax + b\\) はインライン数式の例です.
また，
\\[Y^2 Z + a\_{1} XYZ + a\_{3} YZ^2 = X^{3} + a\_{2} X^{2} Z + a\_{4} X^{2} Z + a\_{6} Z^3.\\]
はディスプレイ数式の例です．
```

たとえば \\(y^2 = x^3 + ax + b\\) はインライン数式の例です.
また，
\\[Y^2 Z + a\_{1} XYZ + a\_{3} YZ^2 = X^{3} + a\_{2} X^{2} Z + a\_{4} X^{2} Z + a\_{6} Z^3.\\]
はディスプレイ数式の例です．

### 記事ごとのライセンス
必要なのかはわかりませんが，記事ごとにライセンスを指定できるようにしました．
たとえば Creative Commons Attribution 4.0 International で記事を公開するには Frontmatter に

``` toml
license = "cc-by" # TOML 形式の場合
```

と指定します．
現在，対応しているライセンスは

- Creative Commons Attribution (`cc-by`)
- Creative Commons Attribution-ShareAlike (`cc-by-sa`)
- Creative Commons Attribution-NonCommercial (`cc-by-nc`)

の3つです．

### 投稿・更新日時の表示
About ページなど，投稿日時は表示したくないが更新日時は表示したい記事は Frontmatter で `hidedates` というパラメータを指定することで表示を制御することにしました．
たとえば投稿日時を表示したくない場合は

``` toml
hidedates = ["date"]
```

更新日時を表示したくない場合は

``` toml
hidedates = ["lastmod"]
```

どちらも表示したくない場合は

``` toml
hidedates = ["date", "lastmod"]
```

とします．更新日時 (`.Lastmod`) の値の取得に関しては [Configure Dates](https://gohugo.io/getting-started/configuration/#configure-dates) を参照してください．
CI を用いてサイトをビルドする場合，`enableGitInfo = true` はうまく行かないことがあるので注意が必要です．
たとえば私が利用している Docker イメージには git がインストールされていないためビルドに失敗します．

### その他
Markdown ファイルは作成日時を含めたファイル名 (e.g., `YYYY-MM-DD-title.md`) にしたいのですが，公開する URL は `presche.me/posts/YYYY/MM/title` という形式にしたいので `slug` を利用しています．
`slug` の値は `archetypes/posts.md` でファイル名から自動的に取得するようにします．

ファイルの作成には簡単な[シェルスクリプト](https://gitlab.com/mahito1594/mahito1594.gitlab.io/snippets/1911138)を利用しているのですが，暇のあるときに改良してファイル名のリネームなどもできるようにしたいところです．

[^1]: あるいは改悪
