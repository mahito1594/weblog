+++
title = "Thunderbird + Enigmail を使ってメールを暗号化する"
date = "2020-02-14T00:00:00+09:00"
hidedates = ["lastmod"]
slug =  "encrypt-mail-with-thunderbird"
tags = ["gpg"]
license = "cc-by"
draft = false
+++

GitHub/GitLab で commit に `Verified` マークを出したかったので GnuPG を導入しました．
ついでなので Thunderbird で電子署名付き・暗号化したメールを独自ドメインのアドレスから送信できるようにしてみました．
本記事はその備忘録です．
Homebrew が使える macOS での作業を念頭に置いています．

*NOTE:* 当然ですが備忘録以上の価値はないため興味のある方は各マニュアル等を参照してください．

## GnuPG の導入
まずは [GnuPG](https://gnupg.org/) の導入です．
`brew install gnupg` で良いでしょう．
GnuPG については

- [GNU Privacy Guard 講座](https://gnupg.hclippr.com/main/)
- [gpg (GNU Privacy Gurad) の使い方](https://qiita.com/moutend/items/5c22d6e57a74845578f6)

などが参考になります．
最近のバージョンでは簡単にプライマリキーの秘密鍵が削除できるようです．
例えば

``` shell
gpg --list-key --show-keygrip
```

で Keygrip を確認して

``` shell
rm ~/.gnupg/private-keys-v1.d/<Keygrip>.key
```

で秘密鍵が削除できます．


## Thunderbird の設定
### メールサーバ
独自ドメインのメールアドレスを作成するにあたって[さくらのメールボックス](https://www.sakura.ne.jp/mail/)を利用しました．
他にも[カゴヤ](https://www.kagoya.jp/multi-plan/mail/)のサービスなども良いと思います[^1]．

独自ドメイン (`presche.me`) 自体は[エックスドメイン](https://www.xdomain.ne.jp)で管理しているため次のように DNS レコードを編集します:

- ホスト名: `presche.me`
- タイプ: `MX`
- コンテンツ: `<初期ドメイン>`
- 優先度: `10`

また SPF の設定もします:

- ホスト名: `presche.me`
- タイプ: `TXT`
- コンテンツ: `v=spf1 +a:www****.sakura.ne.jp +mx ~all`

ここで `www****.sakura.ne.jp` はさくらのサーバパネルから確認できます (`****` には数字 4 桁が入る[^2])．
設定が終わったら `dig` や `nslookup` で確認します．

### Thunderbird
Thunderbird を利用するための設定が[さくらのヘルプページ](https://help.sakura.ad.jp/206054182/)にありますが，
なぜかうまく接続できなかったため，以下のように受信・送信サーバの設定をします:

- IMAP
  - ポート: `993`
  - 接続の保護: `SSL/TLS`
  - 認証方法: `通常のパスワード`

- SMTP
  - ポート: `587`
  - 接続の保護: `STARTTLS`
  - 認証方法: `暗号化パスワード`

設定後，確認のため自分にメールを送受信します．

### Enigmail
Thunderbird でメールの電子署名・暗号化を行うには [Enigmail](https://www.enigmail.net/index.php/en/) というアドオンを利用します[^3]．

基本的には[こちらの記事](https://support.mozilla.org/ja/kb/digitally-signing-and-encrypting-messages)の通りにやれば問題は無いでしょう．
私の場合はすでに GPG Key が存在していたのですぐにセットアップが終わりました．

ただし，電子署名あるいは暗号化時にグラフィカルな `pinentry` が必要になることに注意しておきます．
macOS の場合は `pinentry-mac` を利用すればよいでしょう:

``` shell
brew install pinentry-mac
echo 'pinentry-program /usr/local/bin/pinentry-mac' >> ~/.gnupg/gpg-agent.conf
```

## Tips
[OpenPGP Best Practices](https://riseup.net/en/security/message-security/openpgp/gpg-best-practices).

[^1]: 値段が安いさくらのメールボックスを選択しましたが，使い心地しだいでは他サービスに移行するかもしれません

[^2]: 私の場合は `www2800.sakura.ne.jp`

[^3]: Thunderbird 78 以降では OpenPGP が標準サポートされるようです ([参考](https://forest.watch.impress.co.jp/docs/news/1211933.html))
