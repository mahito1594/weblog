+++
title = "丹野 真人"
date = "2023-02-05"
hidedates = ["date"]
draft = false
+++

東京でプログラムを書いています。
詳しくはこちらへ: [About](https://presche.me)

### Contact
- mahito[at]presche.me
- pb94.mahito[at]gmail.com

(`[at]` を `@` に置き換えてください)
